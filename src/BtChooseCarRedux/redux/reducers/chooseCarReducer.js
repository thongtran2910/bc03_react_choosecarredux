import { DOI_XE } from "../constants/constant";

let initialState = {
  urlImg: "./CarBasic/products/black-car.jpg",
  imgArr: [
    "./CarBasic/products/black-car.jpg",
    "./CarBasic/products/red-car.jpg",
    "./CarBasic/products/silver-car.jpg",
    "./CarBasic/products/steel-car.jpg",
  ],
};

export const chooseCarReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case DOI_XE: {
      state.urlImg = payload;
      return { ...state };
    }
    default:
      return state;
  }
};
