import React, { Component } from "react";
import { connect } from "react-redux";
import { DOI_XE } from "./redux/constants/constant";

class BtChooseCarRedux extends Component {
  render() {
    return (
      <div className="row my-5 mx-5 align-items-center">
        <div className="col-8">
          <img className="img-fluid" src={this.props.hinhXe} alt="" />
        </div>

        <div className="col-4 d-flex flex-column">
          <div
            onClick={() => {
              this.props.handleChangeCar("./CarBasic/products/black-car.jpg");
            }}
            className="btn d-flex flex border border-dark align-items-center my-2"
          >
            <img
              className="w-25 py-2 mx-2"
              src="./CarBasic/icons/icon-black.jpg"
              alt=""
            />
            <div className="w-75 text-left">
              <h3>Crystal Black</h3>
              <p>Pearl</p>
            </div>
          </div>
          <div
            onClick={() => {
              this.props.handleChangeCar("./CarBasic/products/steel-car.jpg");
            }}
            className="btn d-flex flex border border-dark align-items-center my-2"
          >
            <img
              className="w-25 py-2 mx-2"
              src="./CarBasic/icons/icon-steel.jpg"
              alt=""
            />
            <div className="w-75 text-left">
              <h3>Modern Steel</h3>
              <p>Metallic</p>
            </div>
          </div>
          <div
            onClick={() => {
              this.props.handleChangeCar("./CarBasic/products/silver-car.jpg");
            }}
            className="btn d-flex flex border border-dark align-items-center my-2"
          >
            <img
              className="w-25 py-2 mx-2"
              src="./CarBasic/icons/icon-silver.jpg"
              alt=""
            />
            <div className="w-75 text-left">
              <h3>Lunar Silver</h3>
              <p>Metallic</p>
            </div>
          </div>
          <div
            onClick={() => {
              this.props.handleChangeCar("./CarBasic/products/red-car.jpg");
            }}
            className="btn d-flex flex border border-dark align-items-center my-2"
          >
            <img
              className="w-25 py-2 mx-2"
              src="./CarBasic/icons/icon-red.jpg"
              alt=""
            />
            <div className="w-75 text-left">
              <h3>Rallye Red</h3>
              <p>Metallic</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    hinhXe: state.chooseCarReducer.urlImg,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeCar: (url) => {
      dispatch({
        type: DOI_XE,
        payload: url,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BtChooseCarRedux);
