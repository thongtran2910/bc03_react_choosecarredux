import logo from "./logo.svg";
import "./App.css";
import BtChooseCarRedux from "./BtChooseCarRedux/BtChooseCarRedux";

function App() {
  return (
    <div className="App">
      <BtChooseCarRedux />
    </div>
  );
}

export default App;
